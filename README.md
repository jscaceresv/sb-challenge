# SB Challenge: Pico y Placa Predictor [Python]

_Pico y Placa_ predictor given the license plate, date and time following the law stablished in Quito for 2019 prior to the COVID-19 pandemic (Hours: 7:00am - 9:30am / 16:00pm - 19:30)

- `main.py` shows if a car can be on road for the given command line aguments, sample execution as follows (note that _date_ argument should be in  **Y-m-d** format and _time_ argument in  **H:M** format):

`python main.py OBA-7971 2020-09-28 06:59`

- `PicoPlaca.py` implements the logic to determine if a license plate can be on road based on _Pico y Placa_ rules
- Tests can be included in  `tests_unittest.py`

# To Do

- Support various date and time input formats

- Handle errors and exceptions when date/time string format is incorrect or arguments are missing when using `main.py`