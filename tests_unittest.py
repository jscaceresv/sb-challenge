import unittest
from PicoYPlaca.PicoPlaca import PicoPlaca as pp


class TestPicoPlaca(unittest.TestCase):

    def test_1(self):
        self.assertEqual(pp('ABC-1231','2020-09-28','06:59').predictOnRoad(), True, "Should be TRUE")

    def test_2(self):
        self.assertEqual(pp('ABC-1231','2020-09-28','07:00').predictOnRoad(), False, "Should be FALSE")

    def test_4(self):
        self.assertEqual(pp('ABC-1232','2020-09-28','19:25').predictOnRoad(), False, "Should be False")

    def test_5(self):
        self.assertEqual(pp('ABC-1232','2020-09-28','19:45').predictOnRoad(), True, "Should be TRUE")

    def test_6(self):
        self.assertEqual(pp('ABC-1231','2020-09-27','07:00').predictOnRoad(), True, "Should be FALSE")

if __name__ == '__main__':
    unittest.main()
