import sys
from PicoYPlaca.PicoPlaca import PicoPlaca

def main():
    # Creates PicoPlaca object from command line arguments 
    # and evaluates if license_plate can be on road or not
    pp = PicoPlaca(sys.argv[1],sys.argv[2],sys.argv[3])
    can_be_on_road=pp.predictOnRoad()
    if can_be_on_road:
        print("Car can be on road during the specified time")
    else:
        print("Car can't be on road during the specified time")

if __name__ == '__main__':
  main()
