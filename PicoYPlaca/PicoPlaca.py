import datetime

class PicoPlaca():
    
    #pico y placa dictionary for corresponding  day : plate_last_number
    pp_day = {0 : [1, 2], 1 : [3, 4], 2 : [5, 6], 3 : [7, 8], 4 : [9, 0]}   

    #pico y placa restricted timeframes
    morning_start=datetime.time(7, 0)
    morning_end=datetime.time(9, 30)
    afternoon_start=datetime.time(16, 0)
    afternoon_end=datetime.time(19, 30)
    
    def __init__(self, license_plate, date, time):
        self.license_plate = license_plate
        self.date_time = datetime. datetime.strptime(date + " " + time, "%Y-%m-%d %H:%M") 

    def predictOnRoad(self): 
        last_digit = int(self.license_plate[-1])
        day = self.date_time.weekday()
        #if weekend return true as any car can be on road at any hour
        if day in [5,6]:
            return True
        #if plate is in corresponding pico y placa day should check time, otherwise it can be on road for any other day
        if last_digit in self.pp_day[day] :
            if (self.date_time.time() >= self.morning_start and self.date_time.time() <= self.morning_end) \
               or (self.date_time.time() >= self.afternoon_start and self.date_time.time() <= self.afternoon_end):
                return False
            else:
                return True
        else:
            return True
    
